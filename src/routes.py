"""API route declarations

Imports any Flask resources and registers them as API routes to accept
requests and return responses on the Flask server.
"""
from flask import Blueprint, current_app, jsonify
from flask_restful import Api, Resource


def register_routes(_app):
    """Registers api resources/routes with Flask app

    Args:
        _app (object): Flask app object

    """
    from src.resources.subscriptions import (
        SubscriptionAPI,
        SubscriptionListAPI,
        SubscriptionActivateAPI,
        SubscriptionUpdatePlanAPI,
        VersionListBySubscriptionAPI,
    )
    from src.resources.subscription_plans import (
        SubscriptionPlansAPI,
        SubscriptionPlansStatusAPI
    )

    api_blueprint = Blueprint("api", __name__)
    api = Api(api_blueprint, catch_all_404s=False)

    api.add_resource(
        SubscriptionAPI, "/subscription/<int:sid>/", strict_slashes=False)
    api.add_resource(
        SubscriptionListAPI, "/subscriptions/", strict_slashes=False)

    api.add_resource(
        SubscriptionActivateAPI, "/subscription/<int:sid>/activate/", strict_slashes=False)

    api.add_resource(
        SubscriptionUpdatePlanAPI, "/subscription/<int:sid>/update-plan/", strict_slashes=False)

    api.add_resource(
        VersionListBySubscriptionAPI, "/subscription/<int:sid>/versions/")

    api.add_resource(
        SubscriptionPlansAPI, "/subscription-plans/<int:bcid>/")

    api.add_resource(
        SubscriptionPlansStatusAPI, "/subscription-plans-status/<task_id>/", endpoint="taskstatus")


    _app.register_blueprint(api_blueprint, url_prefix="/api")
