"""Subscription resource for handling any subscription requests"""
from flask import jsonify, request
from webargs import fields
from webargs.flaskparser import use_kwargs
from flask_restful import Resource
import json

from src.models.subscriptions import Subscription, Plan
from src.models.utils import get_object_or_404
from src.schemas.subscriptions import SubscriptionSchema
from src.schemas.service_codes import UpdatePlanSchema, ServiceCodeSchema
from src.schemas.versions import VersionSchema


class SubscriptionAPI(Resource):
    """Resource/routes for subscription endpoints"""

    def get(self, sid):
        """External facing subscription endpoint GET

        Gets an existing Subscription object by id

        Args:
            sid (int): id of subscription object

        Returns:
            json: serialized subscription object

        """
        subscription = get_object_or_404(Subscription, sid)
        result = SubscriptionSchema().dump(subscription)
        return jsonify(result.data)


class SubscriptionActivateAPI(Resource):
    def post(self, sid):
        subscription = get_object_or_404(Subscription, sid)
        try:
            subscription.activate()
            result = SubscriptionSchema().dump(subscription)
            return jsonify(result.data)
        except Subscription.IsNotNewError as err:
            data = {"error": err.message}
            response = jsonify(data)
            response.status_code = 400
            return response


class SubscriptionUpdatePlanAPI(Resource):
    def post(self, sid):
        request_body = request.get_json()
        update_plan_schema = UpdatePlanSchema().load(request_body)

        subscription = get_object_or_404(Subscription, sid)
        try:
            subscription.set_new_plan(
                update_plan_schema.data['plan_id']
            )
        except Subscription.SuspendedError as err:
            service_codes = ServiceCodeSchema(
                many=True
            ).dump(err.service_code).data
            data = {
                "error": err.message,
                "status_codes": service_codes
            }
            response = jsonify(data)
            response.status_code = 400
            return response
        except (
                Subscription.ExpiredError, Plan.DoesNotExist,
                Subscription.NotEnoughMemoryError, Subscription.PlanEndedError,
                Plan.UnlimitedError
        ) as err:
            data = {
                "error": err.message
            }
            response = jsonify(data)
            response.status_code = 400
            return response
        return jsonify(update_plan_schema.data)


class VersionListBySubscriptionAPI(Resource):
    def get(self, sid):
        subscription = get_object_or_404(Subscription, sid)
        versions = subscription.versions

        result = VersionSchema(many=True).dump(versions)
        return jsonify(result.data)


class SubscriptionListAPI(Resource):
    """Resource/routes for subscriptions endpoints"""

    @use_kwargs(SubscriptionSchema(partial=True), locations=("query",))
    def get(self, **kwargs):
        """External facing subscription list endpoint GET

        Gets a list of Subscription object with given args

        Args:
            kwargs (dict): filters to apply to query Subscriptions

        Returns:
            json: serialized list of Subscription objects

        """
        subscriptions = Subscription.get_subscriptions(**kwargs)
        result = SubscriptionSchema().dump(subscriptions, many=True)
        return jsonify(result.data)
