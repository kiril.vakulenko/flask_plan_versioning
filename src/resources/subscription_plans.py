from flask import jsonify, request
from flask import url_for

from flask_restful import Resource

from src.tasks.plans import query_subscription_plans


class SubscriptionPlansAPI(Resource):
    def get(self, bcid):
        sid = request.args.get('subscription_id')
        task = query_subscription_plans.delay(bcid, sid)
        response = jsonify({})
        response.status_code = 202
        response.headers = {
            'Location': url_for(
                'api.taskstatus', task_id=task.id
            )
        }
        return response


class SubscriptionPlansStatusAPI(Resource):
    def get(self, task_id):
        task = query_subscription_plans.AsyncResult(task_id)
        if task.state == "PENDING":
            data = {
                'state': task.state
            }
            return jsonify(data)
        if task.state == "FAILURE":
            data = {
                'state': task.state,
                'error': task.info.message
            }
            return jsonify(data)

        return task.info
