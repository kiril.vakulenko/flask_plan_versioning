"""Utilities for models to inherit or use"""
from sqlalchemy.exc import SQLAlchemyError
from flask import abort
from datetime import datetime
from http import HTTPStatus


def get_object_or_404(model, mid):
    """Get an object by id or return a 404 not found response

    Args:
        model (object): object's model class
        mid (int): object's id

    Returns:
        object: returned from query

    Raises:
        404: if one object is returned from query

    """
    try:
        return model.query.filter(model.id == mid).one()
    except SQLAlchemyError:
        abort(404)


def get_expiry_date(start_date):
    year = start_date.year
    month = start_date.month
    day = start_date.day

    expiry_date = datetime(year, month + 1, day)

    return expiry_date
