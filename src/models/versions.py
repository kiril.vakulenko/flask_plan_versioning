"""Version related models and database functionality"""
from src.models.base import db
from datetime import datetime


class Version(db.Model):
    """Model class to represent subscriptions and plan versions"""

    __tablename__ = "versions"

    id = db.Column(db.Integer, primary_key=True)
    creation_date = db.Column(db.TIMESTAMP(timezone=True), default=datetime.utcnow())
    effective_start_date = db.Column(db.TIMESTAMP(timezone=True))
    effective_end_date = db.Column(db.TIMESTAMP(timezone=True), nullable=True)

    subscription_id = db.Column(db.Integer, db.ForeignKey("subscriptions.id"), nullable=False)
    subscription = db.relationship("Subscription", back_populates="versions")

    plan_id = db.Column(db.String(30), db.ForeignKey("plans.id"), nullable=False)
    plan = db.relationship("Plan", foreign_keys=[plan_id], lazy="select")

    def __repr__(self):
        return (
            f"<{self.__class__.__name__}: {self.id} ({self.subscription_id}), "
            f"plan: {self.plan_id}, "
            f"creation_date: {self.creation_date}>"
        )
