"""Subscription related models and database functionality"""
from datetime import datetime
from enum import Enum

from sqlalchemy.dialects.postgresql import ENUM
from sqlalchemy import desc

from src.models.base import db, AttBaseError
from src.models.utils import get_expiry_date
from src.models.service_codes import ServiceCode, subscriptions_service_codes
from src.models.usages import DataUsage
from src.models.versions import Version
from src.models.service_codes import Plan
from src.models.cycles import BillingCycle


class SubscriptionStatus(Enum):
    """Enum representing possible subscription statuses"""
    new = "new"
    active = "active"
    suspended = "suspended"
    expired = "expired"


class Subscription(db.Model):
    """Model class to represent ATT subscriptions"""

    class ExpiredError(AttBaseError):
        pass

    class IsNotNewError(AttBaseError):
        pass

    class NotEnoughMemoryError(AttBaseError):
        pass

    class PlanEndedError(AttBaseError):
        pass

    class DoesNotExist(AttBaseError):
        pass

    class SuspendedError(AttBaseError):
        def __init__(self, message, service_codes):
            super().__init__(message)
            self.service_code = service_codes

    __tablename__ = "subscriptions"

    id = db.Column(db.Integer, primary_key=True)
    phone_number = db.Column(db.String(10))
    status = db.Column(ENUM(SubscriptionStatus), default=SubscriptionStatus.new)
    activation_date = db.Column(db.TIMESTAMP(timezone=True), nullable=True)
    expiry_date = db.Column(db.TIMESTAMP(timezone=True), nullable=True)

    plan_id = db.Column(db.String(30), db.ForeignKey("plans.id"), nullable=False)
    plan = db.relationship("Plan", foreign_keys=[plan_id], lazy="select")
    service_codes = db.relationship(
        "ServiceCode", secondary=subscriptions_service_codes,
        primaryjoin="Subscription.id==subscriptions_service_codes.c.subscription_id",
        secondaryjoin="ServiceCode.id==subscriptions_service_codes.c.service_code_id",
        back_populates="subscriptions", cascade="all,delete", lazy="subquery"
    )

    data_usages = db.relationship(DataUsage, back_populates="subscription")

    versions = db.relationship(Version, back_populates="subscription")

    def __repr__(self):  # pragma: no cover
        return (
            f"<{self.__class__.__name__}: {self.id} ({self.status}), "
            f"phone_number: {self.phone_number or '[no phone number]'}, ",
            f"plan: {self.plan_id}>"
        )

    @classmethod
    def get_subscriptions(cls, **kwargs):
        """Gets a list of Subscription objects using given kwargs

        Generates query filters from kwargs param using base class method

        Args:
            kwargs: key value pairs to apply as filters

        Returns:
            list: objects returned from query result

        """
        return cls.query.filter(**kwargs).all()

    @property
    def service_code_names(self):
        """Helper property to return names of active service codes"""
        return [code.name for code in self.service_codes]

    def activate(self):
        if self.status != SubscriptionStatus.new:
            raise Subscription.IsNotNewError(
                'subscription is not new'
            )
        self.activation_date = datetime.now()
        self.status = SubscriptionStatus.active
        self.expiry_date = get_expiry_date(self.activation_date)
        effective_start_date = self.activation_date
        self._add_new_version(effective_start_date, self.plan)

    def set_new_plan(self, plan_id):
        if self.status == SubscriptionStatus.expired:
            raise Subscription.ExpiredError(
                "subscription expired"
            )
        if self.status == SubscriptionStatus.suspended:
            raise Subscription.SuspendedError(
                "subscription is suspended",
                self.service_codes
            )

        plan_exists = db.session.query(
            Plan.id
        ).filter_by(id=plan_id).scalar() is not None
        if not plan_exists:
            raise Plan.DoesNotExist(
                f'plan with id {plan_id} does not exist'
            )
        new_plan = Plan.query.filter_by(id=plan_id).one()

        if self.status == SubscriptionStatus.active:
            self._change_plan_in_active_state(new_plan)

        elif self.status == SubscriptionStatus.new:
            self._change_plan_in_new_state(new_plan)

    def _change_plan_in_new_state(self, new_plan):
        self.plan = new_plan
        db.session.commit()

    def _change_plan_in_active_state(self, new_plan):
        if self.plan.mb_available <= new_plan.mb_available:
            self._set_bigger_plan(new_plan)
        else:
            self._set_smaller_plan(new_plan)

    def _set_bigger_plan(self, new_plan):
        if self.plan.is_unlimited:
            raise Plan.UnlimitedError(
                'plan is unlimited'
            )

        effective_start_date = datetime.utcnow()
        last_version = self.get_last_version()
        self._add_new_version(effective_start_date, new_plan, last_version)

    def _set_smaller_plan(self, new_plan):
        last_version = self.get_last_version()
        if last_version.effective_end_date <= datetime.utcnow():
            raise Subscription.PlanEndedError(
                "plan effective end date has already come"
            )

        if not self._is_enough_memory_to_backdate(new_plan):
            raise Subscription.NotEnoughMemoryError(
                "not enough memory left to backdate"
            )

        effective_start_date = self.activation_date
        self._add_new_version(effective_start_date, new_plan, last_version)

    def get_last_version(self):
        return sorted(self.versions, key=lambda v: v.creation_date)[-1]

    def _is_enough_memory_to_backdate(self, new_plan):
        total_mb_used = db.session.query(
            db.func.sum(DataUsage.mb_used)
        ).filter(
            DataUsage.subscription_id == self.id
        ).one()[0]
        print(total_mb_used)
        if total_mb_used >= new_plan.mb_available:
            return False
        return True

    def _add_new_version(self, effective_start_date, new_plan, last_version=None):
        if last_version:
            last_version.effective_end_date = datetime.utcnow()
            self.plan = new_plan

        billing_cycle = BillingCycle.get_current_cycle()

        new_version = Version(
            effective_start_date=effective_start_date,
            effective_end_date=billing_cycle.end_date,
            plan=new_plan,
            subscription=self
        )
        db.session.add(new_version)
        db.session.commit()
