from marshmallow import fields, Schema


class SubscriptionPlansSchema(Schema):
    mb_available = fields.Int()

    effective_start_date = fields.DateTime()
    effective_end_date = fields.DateTime()
