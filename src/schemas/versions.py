"""Version schemas to access from subscription url"""
from marshmallow import fields, Schema

from src.schemas.service_codes import PlanSchema


class VersionSchema(Schema):
    id = fields.Integer()

    creation_date = fields.DateTime()
    effective_start_date = fields.DateTime()
    effective_end_date = fields.DateTime()

    plan_id = fields.String()
    plan = fields.Nested(PlanSchema, dump_only=True)
