"""Plan related tasks"""
from celery.utils.log import get_task_logger
from flask import current_app
from flask import jsonify

from sqlalchemy import and_

from src.celery_app import celery
from src.models.subscriptions import Subscription
from src.models.cycles import BillingCycle
from src.models.versions import Version
from src.models.service_codes import Plan
from src.models.base import db
from src.schemas.subscription_plans import SubscriptionPlansSchema


log = get_task_logger(__name__)


@celery.task
def query_subscription_plans(billing_cycle_id, subscription_id=None):
    """Searches for plans that where effective during a given billing cycle
    for a given subscription if `subscription_id` is provided.
    If only `billing_cycle_id` is provided searches for effective plans
    for all subscriptions.

        Args:
            billing_cycle_id (int): billing cycle id in which to search
            subscription_id (int): id of a subscription for which to search

        Returns:
            dict:
                {
                    'subscription_plans': list(SubscriptionPlansSchema)
                }
            if `subscription_id` is provided else
            dict:
                {
                    `mb_available`: list(int)
                }

        Raises:
            BillingCycle.DoesNotExist: If billing cycle with id `billing_cycle_id`
                does not exist
            Subscription.DoesNotExist: If subscription with id `subscription_id`
                does not exist
    """

    if subscription_id is not None:
        subscription_exists = db.session.query(
            Subscription.id
        ).filter_by(
            id=subscription_id
        ).scalar() is not None

    billing_cycle_exists = db.session.query(
        BillingCycle.id
    ).filter_by(
        id=billing_cycle_id
    ).scalar() is not None

    if not billing_cycle_exists:
        raise BillingCycle.DoesNotExist(
            f"billing cycle with id {billing_cycle_id} does not exist"
        )
    if subscription_id is not None and not subscription_exists:
        raise Subscription.DoesNotExist(
            f"subscription with id {subscription_id} does not exist"
        )

    billing_cycle = BillingCycle.query.filter(BillingCycle.id == billing_cycle_id).one()
    if subscription_id is None:
        return _query_all_subscriptions(billing_cycle)
    return _query_subscription(subscription_id, billing_cycle)


def _query_subscription(subscription_id, billing_cycle):
    subquery = db.session.query(
        Version.effective_start_date,
        db.func.max(Version.creation_date).label('creation_date')
    ).filter(
        Version.subscription_id == subscription_id,
        Version.effective_start_date >= billing_cycle.start_date,
        Version.effective_end_date <= billing_cycle.end_date
    ).group_by(Version.effective_start_date).subquery()
    query = db.session.query(
        Plan.mb_available,
        Version.effective_start_date,
        Version.effective_end_date
    ).join(Plan).join(
        subquery, and_(
            Version.creation_date == subquery.c.creation_date,
            Version.effective_start_date == subquery.c.effective_start_date
        )
    )
    res = query.all()
    subscription_plans = SubscriptionPlansSchema(many=True).dump(res)
    return {
        "subscription_plans": subscription_plans.data
    }


def _query_all_subscriptions(billing_cycle):
    subquery = db.session.query(
        Version.subscription_id,
        Version.effective_start_date,
        db.func.max(Version.creation_date).label('creation_date')
    ).filter(
        Version.effective_start_date >= billing_cycle.start_date,
        Version.effective_end_date <= billing_cycle.end_date
    ).group_by(Version.effective_start_date, Version.subscription_id).subquery()
    query = db.session.query(
        Version.subscription_id,
        Plan.mb_available
    ).join(Plan).join(
        subquery, and_(
            Version.creation_date == subquery.c.creation_date,
            Version.effective_start_date == subquery.c.effective_start_date,
            Version.subscription_id == subquery.c.subscription_id
        )
    )
    data = {}
    for row in query.all():
        _insert_into_data(
            data=data,
            subscription_id=row[0],
            mb_available=row[1]
        )

    return data


def _insert_into_data(data, subscription_id, mb_available):
    if mb_available in data:
        data[mb_available].append(subscription_id)
        return
    data[mb_available] = []
    data[mb_available].append(subscription_id)
