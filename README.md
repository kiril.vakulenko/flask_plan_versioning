# Coding Challenge 3

## Requirements
* Python 3.5+
* SQLite

## Starting the server
Everything is set up, including the database and some seed data. All you need to do is create the virtual environment with the dependencies and then execute `flask run`. Voila!

## Challenge
1. Add a versioning table for subscriptions and plan so we can keep track of changes.
    - This should have fields to represent the subscription, plan, starting effective date of the plan, ending effective date of the plan, and a date the version was created.
    - Plans have the ability to be backdated within a billing cycle. So, for instance, a subscription might start on a 5GB plan at the beginning of the cycle, but if we see toward the end of the cycle that their usage is under 1GB, we might backdate their plan to a 1GB with an effective date of the beginning of the cycle.
    - We need to keep track of each plan change, even if we backdate to the beginning of the cycle. Using the example above, there should be a table entry for a 5GB plan AND an try for the 1GB plan for that sub for the billing cycle.
    - For most subscriptions, the starting effective date of the plan and the ending effective date of the plan will be the start and end of the billing cycle.
    - For subscriptions that are activated mid-cycle, the plan can only be backdated as far as the date the subscription was activated. So the starting effective date would be the date of activation.
    - Similarly, for subscriptions that are expired mid-cycle, the plan can only be effective as long as the subscription was active. So the ending effective date would be the date of expiry.
2. Add code for the `query_subscription_plans` task in the `plans.py` tasks file. This should do the following:
    - Accept a subscription id and billing cycle id as parameters.
    - Query the subscriptions plans version table for the given subscription id in the given billing cycle.
    - Determine what plan the subscription was effective for what dates (starting effective date and ending effective date)
        - For the example above with the 5GB plan and 1GB plan for a subscription, it should only return the 1GB plan with a starting effective date of the beginning of the cycle and ending effective date of the end of the cycle.
    - Return the results as a list of tuples that include the plan size, start effective date and end effective date.

## Bonuses
- For part 2 of the challenge, allow a billing cycle id as a parameter without a subscription id. If a subscription id isn't passed in, then query for any subscriptions that were active at any point during the given billing cycle. Return the results in a dict format with the plan size as the key and a list of the subscription ids as the value (don't worry about passing back the effective dates).
- Extra points for efficiency
- Add to or modify the schema to optimize usage queries
- Improve the existing code in general